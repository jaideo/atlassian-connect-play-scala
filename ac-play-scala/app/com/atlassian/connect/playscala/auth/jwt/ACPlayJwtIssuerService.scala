package com.atlassian.connect.playscala.auth.jwt

import com.atlassian.jwt.core.reader.{ JwtIssuerValidator, JwtIssuerSharedSecretService }
import com.atlassian.connect.playscala.model.ClientKey
import play.Logger
import com.atlassian.connect.playscala.store.DbConfiguration

class ACPlayJwtIssuerService(dbConfiguration: DbConfiguration) extends JwtIssuerSharedSecretService with JwtIssuerValidator {

  override def getSharedSecret(issuer: String): String =
    dbConfiguration.acHostModelStore.findByKey(ClientKey(issuer)) map { host =>
      val sharedSecret = host.sharedSecret
      if (sharedSecret == null) {
        Logger.warn(s"The issuer $issuer does not have a shared secret in the database. Please reinstall the addon in this host")
      }
      sharedSecret
    } getOrElse {
      Logger.warn(s"The issuer $issuer cannot be found in the database. It might a host that has not registered")
      null
    }

  override def isValid(issuer: String): Boolean = issuer != null
}
