package com.atlassian.connect.playscala
package auth

import com.atlassian.connect.playscala.model.AcHostModel
import org.apache.commons.codec.binary.Base64
import play.api.libs.Crypto
import play.api.libs.json.{ Writes, Reads, Json }
import play.api.Logger
import scalaz.syntax.std.option._

case class Token(acHost: AcHostModel, userContext: Option[UserContext], allowInsecurePolling: Boolean = false, timestamp: Long = System.currentTimeMillis()) {
  lazy val encryptedToken: String = {
    import Token._
    val compressed = CompressedToken(acHost.key.value, userContext, 1.some ifOnly allowInsecurePolling, timestamp)
    val jsonToken = Base64 encodeBase64String Json.toJson(compressed).toString().getBytes
    val result = Crypto encryptAES jsonToken
    Logger.debug(s"Encrypted $compressed to $result")
    result
  }
}

case class UserInfo(userKey: String, userName: String, userDisplayName: String)

case class UserContext(jwtSubject: String, user: Option[UserInfo])

case class CompressedToken(h: String, uc: Option[UserContext], p: Option[Int], t: Long)

object Token {

  val PageTokenParam: String = "acpt"
  val PageTokenHeader: String = "X-" + PageTokenParam

  implicit val userInfoReads: Reads[UserInfo] = Json.reads[UserInfo]
  implicit val userInfoWrites: Writes[UserInfo] = Json.writes[UserInfo]

  implicit val userContextReads: Reads[UserContext] = Json.reads[UserContext]
  implicit val userContextWrites: Writes[UserContext] = Json.writes[UserContext]

  implicit val compressTokenReads: Reads[CompressedToken] = Json.reads[CompressedToken]
  implicit val compressTokenWrites: Writes[CompressedToken] = Json.writes[CompressedToken]
}

